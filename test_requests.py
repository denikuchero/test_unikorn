import requests as requests

response = requests.get('http://localhost:8081/usd/get')
response_2 = requests.get('http://localhost:8081/rub/get')
response_3 = requests.get('http://localhost:8081/eur/get')
response_4 = requests.get('http://localhost:8081/amount/get')

response_post = requests.post('http://localhost:8081/amount/set', data={"usd":10})
response_post_2 = requests.post('http://localhost:8081/amount/set', data={"rub":100.5, "eur":10, "usd":20})
response_post_3 = requests.post('http://localhost:8081/modify', data={"usd":5})
response_post_4 = requests.post('http://localhost:8081/modify', data={"eur":10, "rub":-20})

print(response)
print(response.text)
print(response_2.text)
print(response_3.text)
print(response_4.text)

response_post = requests.post('http://localhost:8081/amount/set', params={"rub":35000, "eur":1000, "usd":20})
print(response_post.text)

