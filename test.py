import argparse
import asyncio
import json
from io import StringIO, BytesIO

from aiohttp.web import Application, run_app
from aiohttp import web

import aiohttp
import logging

from aiohttp.web_response import Response
url = 'https://www.cbr-xml-daily.ru/daily_json.js'

class ParseCourseValue:
    def __init__(self):
        self.currency = {'rub': 0, 'usd': 0 , 'eur': 0 }
        self.change = {'rub_usd': 0, 'rub_eur': 0,'usd_eur': 0}
        self.buffer_currency = {}
        self.buffer_change = {}
        self.summ = {'rub': 0, 'usd': 0 , 'eur': 0}
        self.period = 1
        self.debug = False


    async def session(self, url):
        logging.info(' делаем запрос ')
        while True:
            async with aiohttp.ClientSession() as session:
                response = await self.response_cbr(session, url)
                print('Получили данные о курсе валют')
                print('Баланс', self.currency)

                self.change['rub_usd'] = float(response.get('Valute').get('USD').get('Value'))
                self.change['rub_eur'] = float(response.get('Valute').get('EUR').get('Value'))
                self.change['usd_eur'] = round(self.change['rub_eur'] / self.change['rub_usd'], 4)

                self.summ['rub'] = self.currency['rub'] + self.currency['usd'] * self.change['rub_usd']  + self.currency['eur'] * self.change['rub_eur']
                self.summ['usd'] = self.currency['usd'] + self.currency['rub'] / self.change['rub_usd']  + self.currency['eur'] * self.change['usd_eur']
                self.summ['eur'] = self.currency['eur'] + self.currency['rub'] / self.change['rub_eur']  + self.currency['eur'] / self.change['usd_eur']

                for key, value in self.summ.items():
                    self.summ[key] = round(value, 2)
            await asyncio.sleep(self.period * 60)


    async def response_cbr(self, session, url):
        """ получаем данные о валютах из центробанка
        """
        while True:
            async with session.get(url) as response:
                assert response.status == 200
                result = await response.text()

                return json.loads(result)

    async def start_server(self, app, address='localhost', port=8080):
        """ старт сервера для обработки запросов
        """
        try:
            runner = web.AppRunner(app)
            await runner.setup()
            site = web.TCPSite(runner, address, port)
            await site.start()
            while True:
                await asyncio.sleep(3600)  # sleep forever

        except:
            await runner.cleanup()

    async def handle_get(self, request):
        """ обработка get запросов
        """
        currency = request.path.split('/')[1]
        if currency in ['rub', 'usd', 'eur']:
            response_obj = {'currency': currency, 'amount': self.currency.get(currency)}
            return Response(text=json.dumps(response_obj), content_type='text/plain',  status=200)
        elif currency in ['amount']:
            response_obj = {'currency': 'amount ', 'amount': self.currency}
            return Response(text=json.dumps(response_obj), content_type='text/plain', status=200)
        else:
            response_obj = {'error': 'в вашем запрое нет валюты'}
            return Response(text=json.dumps(response_obj), content_type='text/plain', status=500)

    async def mixin_post(self,currency, key, value):
        for name_value in self.currency.keys():
            if name_value in key:
                if 'amount' in currency:
                    self.currency[name_value] = float(value)
                elif 'modify' in currency:
                    self.currency[name_value] += float(value)
                    self.currency[name_value] = max(self.currency[name_value], 0)

    async def handle_post(self, request, loads=json.loads):
        currency = request.path.split('/')
        if request.body_exists:
            response = await request.text()
            try:
                response_dict = loads(response)
                for key, value in response_dict.items():
                    await self.mixin_post(currency, key, value)
                response_obj = {'status': 'success', 'currency': self.currency}
                return Response(text=json.dumps(response_obj), content_type='text/plain', status=200)
            except:
                response_list = response.split()
                try:
                    for item, key in enumerate(response_list):
                        if item + 1 < len(response_list):
                            await self.mixin_post(currency, key, response_list[item + 1])
                    response_obj = {'status': 'success', 'currency': self.currency}
                    return Response(text=json.dumps(response_obj), content_type='text/plain', status=200)
                except Exception as e:
                    response_obj = {'status': 'success', 'message': str(e)}
                    return Response(text=json.dumps(response_obj), content_type='text/plain', status=500)

        else:
            response_obj = {'status': 'no success', 'message': 'no data for change'}
            return Response(text=json.dumps(response_obj), content_type='text/plain', status=500)

    async def print_change(self):
        """ Выводим в консоль если изменился баланс или курс валюты
        """
        while True:
            if self.buffer_currency != self.currency:
                print('Изменился баланс')
                self.buffer_currency = self.currency.copy()
            if self.buffer_change != self.change:
                print('Изменился курс валют')
                self.buffer_change = self.change.copy()

            await asyncio.sleep(1*60)


class Worker(ParseCourseValue):

    def __init__(self, args):
        super().__init__()
        self.currency = {'rub': args.rub, 'usd': args.usd, 'eur': args.eur}
        self.period = args.period
        if args.debug in ['1', 'true', 'True', 'y', 'Y']:
            self.debug = True
            logging.basicConfig(level=logging.DEBUG)
        else:
            self.debug = False
            logging.basicConfig(level=logging.WARNING)





if __name__ == '__main__':
    print('start')
    parser = argparse.ArgumentParser()
    parser.add_argument("--rub", help="Количество рублей", type=float, default=0)
    parser.add_argument("--usd", help="Количество долларов", type=float, default=0)
    parser.add_argument("--eur", help="Количество евро", type=float, default=0)
    parser.add_argument("--period", help="Период за который берутся данные", type=int, default=1)
    parser.add_argument("--debug", help="допольнительная информация о дебаге", default=False, choices=['0', '1', 'true', 'false', 'True', 'False', 'y', 'n', 'Y', 'N'])
    args = parser.parse_args()

    g = Worker(args)

    app = Application()
    app.router.add_get('/rub/get', g.handle_get)
    app.router.add_get('/usd/get', g.handle_get)
    app.router.add_get('/eur/get', g.handle_get)
    app.router.add_get('/amount/get', g.handle_get)

    app.router.add_post('/amount/set', g.handle_post)
    app.router.add_post('/modify', g.handle_post)


    loop = asyncio.get_event_loop()
    tasks = [asyncio.Task(g.session(url)), asyncio.Task(g.print_change()), asyncio.Task(g.start_server(app, port=8081))]
    loop.run_until_complete(asyncio.gather(*tasks))


